//
//  ViewController.swift
//  SnapKitDemo
//
//  Created by Edy Cu Tjong on 6/11/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let box = UIView()
        box.backgroundColor = UIColor.black
        self.view.addSubview(box)
        
        // Add constraints
        box.snp_makeConstraints { (make) in
//            make.top.bottom.left.right.equalTo(self.view)
            make.edges.equalTo(self.view)
        }
    }


}

